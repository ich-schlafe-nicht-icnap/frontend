/*eslint-disable*/

import * as React from "react";
import Websocket from "react-websocket";

class WebView extends React.Component {
    constructor(props) {
        super(props);
    }

    handleData(data) {
        let result = JSON.parse(data);
        return "Test";
    }

    render() {
        return (
            <div>
                <Websocket
                    url='ws://localhost:8888/'
                    onMessage={this.handleData.bind(this)}
                />
            </div>
        );
    }
}

export default WebView;
