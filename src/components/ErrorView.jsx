/*eslint-disable*/

import * as React from "react";
import PropTypes from "prop-types";
import "./ErrorView.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { Box, Button } from "react-bulma-components";
import { FaBeer, FaEdit, FaClock } from "react-icons/fa";

class ErrorView extends React.Component {
    render() {
        return (
            <div className='card'>
                <header className='card-header'>
                    <p className='card-header-title'>
                        M12: {this.props.reason} ({this.props.user})
                    </p>
                    <span className='card-header-icon'>
                        <time dateTime='2016-1-1'>11:09 PM - 1 Jan 2016</time>
                    </span>
                </header>
                <div className='card-content'>
                    <div className='content'>
                        <i>{this.props.description}</i>
                    </div>
                </div>
                <footer className='card-footer'>
                    <div className='card-footer-item noborder' />
                    <div className='card-footer-item noborder' />
                    <a
                        href='#'
                        className='card-footer-item'
                        onClick={event =>
                            this.props.setShowPopup({
                                visible: true,
                                reason: this.props.reason,
                                description: this.props.description
                            })
                        }
                    >
                        <FaEdit /> Edit
                    </a>
                    <a href='#' className='card-footer-item'>
                        <FaClock /> In Process
                    </a>
                </footer>
            </div>
        );
    }
}

ErrorView.propTypes = {
    user: PropTypes.string.isRequired,
    machine: PropTypes.number.isRequired,
    time: PropTypes.instanceOf(Date).isRequired,
    reason: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
};

export default ErrorView;
