/*eslint-disable*/
import React from 'react';

class ErrorPopupView extends React.Component {
    render() {
        return (
            <div id="myNav" className="overlay">
                {/* eslint-disable-next-line no-script-url */}
                <a href="javascript:void(0)" className="closebtn" onClick="closeNav()">&times;</a>
                <div className="overlay-content">
                    <a href="#">About</a>
                    <a href="#">Services</a>
                    <a href="#">Clients</a>
                    <a href="#">Contact</a>
                </div>

            </div>
        );
    }

    openNav() {
        document.getElementById('myNav').style.width = '100%';
    }

    closeNav() {
        document.getElementById('myNav').style.width = '0%';
    }
}

export default ErrorPopupView;
