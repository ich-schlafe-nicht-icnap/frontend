/*eslint-disable*/

import * as React from "react";
import PropTypes from "prop-types";
import "./App.css";
import ReactTags from "react-tag-autocomplete";

import ErrorView from "./components/ErrorView.jsx";
import WebView from "./components/WebView";

import logo from "./logo.svg";

import { FaSmile, FaExclamationTriangle } from "react-icons/fa";

import Chart from "react-apexcharts";

class App extends React.Component {
    constructor(props) {
        super(props);
        let date = new Date();
        this.state = {
            options: {
                colors: ["#EF5350", "#00C853"],
                chart: {
                    id: "apexchart-example"
                },
                xaxis: {
                    categories: []
                }
            },
            popup: {
                visible: false,
                description: "",
                reason: "Unknown"
            },
            series: [
                {
                    name: "Stopped",
                    data: []
                },
                {
                    name: "Error",
                    data: []
                }
            ],

            errors: [
                {
                    user: "Peter Mueller",
                    reason: "Technical Problem",
                    description: "Machine jammed during the milling process",
                    time: `${date.getDate()}-${date.getMonth()}-${date.getHours()}:${date.getMinutes()}`
                },
                {
                    user: "Peter Duebel",
                    reason: "Technical Problem",
                    description: "Power outage in the factory",
                    time: `${date.getDate()}-${date.getMonth()}-${date.getHours()}:${date.getMinutes()}`
                },
                {
                    user: "Hermann Schneider",
                    reason: "Logistical Problem",
                    description: "Out of Aluminium",
                    time: "28-9-22:38"
                },
                {
                    user: "Florian Schneider",
                    reason: "Resource Problem",
                    description:
                        "Drill bits were not available to replace broken ones",
                    time: `${date.getDate()}-${date.getMonth()}-${date.getHours()}:${date.getMinutes()}`
                }
            ],
            tags: [],
            suggestions: [
                { isStatus: true, name: "ERROR" },
                { isProblem: true, name: "Technical Problem" },
                { isProblem: true, name: "Logistical Problem" },
                { isProblem: true, name: "Resource Problem" },
                { isProblem: true, name: "Human Error" },
                { isProblem: true, name: "Other Problem" }
            ]
        };
    }

    componentDidMount() {
        fetch('http://127.0.0.1:2298/history/2019-09-01')
         .then(response => {
          return response.json();
         })
         .then(json => {
             return {
             categories: json.map(entry => entry.datetime),
             stopped: json.map(entry => entry.stopped_machines),
             error: json.map(entry => entry.broken_machines),
             };
         })
         .then((data) => {
             const xaxis = { categories: data.categories };
             const options = {...this.state.options, xaxis};
             const series = [
                {   
                    name: "Stopped",
                    data: data.stopped,
                },
                {
                    name: "Error",
                    data: data.error,
                }
             ];

             this.setState({options, series});
        });
    }


    onDelete(i) {
        const tags = this.state.tags.slice(0);
        tags.splice(i, 1);
        this.setState({ tags });
    }

    onAddition(tag) {
        if (
            this.state.tags.reduce(
                (acc, tag1) =>
                    acc || tag1.name.toUpperCase() === tag.name.toUpperCase(),
                false
            )
        ) {
            return;
        }
        const tags = [].concat(this.state.tags, tag);
        this.setState({ tags });
    }

    applyTags(tags) {
        let modified = this.state.errors;
        tags.forEach(tag => {
            modified = modified.filter(
                elem =>
                    elem.user.toUpperCase().includes(tag.name.toUpperCase()) ||
                    elem.reason
                        .toUpperCase()
                        .includes(tag.name.toUpperCase()) ||
                    elem.time.startsWith(tag.name)
            );
        });

        return modified.map(error => (
            <ErrorView
                {...error}
                setShowPopup={popup => this.setState({ popup })}
            />
        ));
    }

    trySaveEdit() {}

    render() {
        let date = new Date();

        return (
            <>
                {this.state.popup.visible && (
                    <div className='modal'>
                        <div className='modal-background'></div>
                        <div className='modal-card'>
                            <header className='modal-card-head'>
                                <p className='modal-card-title'>
                                    <FaExclamationTriangle />
                                    &nbsp; An error occured
                                </p>
                            </header>
                            <section className='modal-card-body'>
                                <label forHtml='reason'>Reason:</label>
                                <br />
                                <div className='select'>
                                    <select
                                        id='reason'
                                        name='reason'
                                        value={this.state.popup.reason}
                                        onChange={ev =>
                                            this.setState({
                                                popup: {
                                                    ...this.state.popup,
                                                    reason: ev.target.value
                                                }
                                            })
                                        }
                                    >
                                        <option>Technical Problem</option>
                                        <option>Logistical Problem</option>
                                        <option>Resource Problem</option>
                                        <option>Human Error</option>
                                        <option>Unknown</option>
                                    </select>
                                </div>
                                <br />
                                <br />
                                <label forHtml='description'>
                                    Description:
                                </label>
                                <textarea
                                    className='textarea'
                                    id='description'
                                    name='description'
                                    placeholder={"Describe the problem here"}
                                    value={this.state.popup.description}
                                    onChange={ev =>
                                        this.setState({
                                            popup: {
                                                ...this.state.popup,
                                                description: ev.target.value
                                            }
                                        })
                                    }
                                ></textarea>
                            </section>
                            <footer className='modal-card-foot'>
                                <button className='button is-success'>
                                    Save changes
                                </button>
                                <button
                                    className='button'
                                    onClick={ev =>
                                        this.setState({
                                            popup: { visible: false }
                                        })
                                    }
                                >
                                    Cancel
                                </button>
                            </footer>
                        </div>
                    </div>
                )}
                <div className='App'>
                    <header>
                        <nav
                            className='navbar'
                            role='navigation'
                            aria-label='main navigation'
                        >
                            <div className='navbar-brand'>
                                <img
                                    src='https://www.wennigercompressor.com/images/Atlas%20Copco%20-%20Logo.jpg'
                                    width='112'
                                    height='28'
                                />

                                <a
                                    role='button'
                                    className='navbar-burger burger'
                                    aria-label='menu'
                                    aria-expanded='false'
                                    data-target='navbarBasicExample'
                                >
                                    <span aria-hidden='true'></span>
                                    <span aria-hidden='true'></span>
                                    <span aria-hidden='true'></span>
                                </a>
                            </div>

                            <div
                                id='navbarBasicExample'
                                className='navbar-menu'
                            >
                                <div className='navbar-start'>
                                    <a className='navbar-item'>
                                        Submitted Problems
                                    </a>
                                    <a className='navbar-item'>Documentation</a>
                                </div>

                                <div className='navbar-end'>
                                    <div className='navbar-item'>
                                        <div className='buttons'>
                                            <a className='button'>
                                                <FaSmile /> Peter Maier
                                            </a>
                                            <a className='button is-primary'>
                                                <strong>Sign out</strong>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                        {true && (
                            <Chart
                                options={this.state.options}
                                series={this.state.series}
                                type='bar'
                                height={300}
                            />
                        )}
                    </header>
                    <ReactTags
                        placeholderText={"Search"}
                        tags={this.state.tags}
                        suggestions={this.state.suggestions.concat(
                            this.state.errors.map(error => ({
                                isOperator: true,
                                name: error.user
                            }))
                        )}
                        onDelete={this.onDelete.bind(this)}
                        onAddition={this.onAddition.bind(this)}
                        allowNew={true}
                    />
                    {this.applyTags(this.state.tags)}
                </div>
            </>
        );
    }
}

App.propTypes = {
    name: PropTypes.string.isRequired
};

export default App;
