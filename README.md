# Webpack 4 React Starter [![Depfu](https://badges.depfu.com/badges/f433e0195bd822de8e1471d06f558b35/count.svg)](https://depfu.com/github/Empty2k12/react-barebones?project=Npm)

This is a React Starter using Webpack 4

### Prerequisites

-   Clone this repository

### Installing

-   Run `npm install` & `npm run` to get the development server up and running

-   Browse to http://localhost:8080/

### Features

-   Webpack 4

-   eslint with Airbnb Rules enabled by default

### Hat Tip

`react-create-app` for lending the basic React barebone

### License

[![WTFPL License](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)](http://www.wtfpl.net/)

**Free Software, Hell Yeah!**
